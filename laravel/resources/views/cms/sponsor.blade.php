@extends('cms.master')

@section('content')
    <div class="col-md-4">
        <h3 class="page-header">Sponsor List</h3>
        <p id="sponsorList">Loading...</p>
        <button type="button" class="btn btn-primary" id="addSponsor">Add Sponsor</button>
    </div>
    <div class="col-md-8">
        <h3 class="page-header">Sponsor Detail</h3>

        {{ Form::open(['action' => 'CmsController@updateSponsor', 'files' => 'true', 'id' => 'sponsorForm', 'class' => 'form-horizontal']) }}

        <div class="form-group">
            <label class="sr-only col-sm-2 control-label">ID</label>
        </div>

        {{-- ID --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">ID</label>
            <div class="col-sm-10">
                <input class="form-control" id="sponsorId" name="sponsorId">
            </div>
        </div>

        {{-- NAME --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Name</label>
            <div class="col-sm-10">
                <input class="form-control" id="sponsorName" name="sponsorName">
            </div>
        </div>

        {{-- Image --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Image</label>
            <div class="col-sm-10">
                <input class="form-control" id="sponsorImage" name="sponsorImage">
            </div>
        </div>

        {{-- UPLOAD IMAGE --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">{!! Form::label('upload', 'Upload') !!}</label>
            <div class="col-sm-10">
                {!! Form::file('upload') !!}
            </div>
        </div>

        {{-- Description --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Description</label>
            <div class="col-sm-10">
                <input class="form-control" id="sponsorDescription" name="sponsorDescription">
            </div>
        </div>

        {{-- Website --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Website</label>
            <div class="col-sm-10">
                <input class="form-control" id="sponsorWebsite" name="sponsorWebsite">
            </div>
        </div>

        {{-- Address --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Address</label>
            <div class="col-sm-10">
                <input class="form-control" id="sponsorAddress" name="sponsorAddress">
            </div>
        </div>

        {{-- Phone --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Phone</label>
            <div class="col-sm-10">
                <input class="form-control" id="sponsorPhone" name="sponsorPhone">
            </div>
        </div>

        {{-- Email --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
                <input class="form-control" id="sponsorEmail" name="sponsorEmail">
            </div>
        </div>

        {{-- BUTTON TO SEND THE FORM AND UPDATE SPONSOR --}}
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Update Sponsor Detail</button>
            </div>
        </div>
        {{ Form::close() }}

    </div>
@endsection

@section('script')
    <script>

        $('#sponsorSidebarItem').addClass("active");

        // Fetch and show list of sponsors
        $.getJSON("../sponsor/all", function (sponsor) {
            console.log("Getting all sponsor...");
            var sponsorListHTMLBody = "";
            $.each(sponsor, function (index, element) {
                console.log("sponsor id: " + index + ", name " + element.name);
                sponsorListHTMLBody += "<li>" +
                        "<a href='#' onclick='reloadSponsorDetail(" + this.id + ")'>" + this.name + "</a> " +
                        "<a href='#' onclick='deleteSponsor(" + this.id + ")'>(Delete)</a>" +
                        "</li>";
            });
            $("#sponsorList").html("<ol>" + sponsorListHTMLBody + "</ol>");
            reloadSponsorDetail(1);
        });

        function deleteSponsor(sponsorId) {
            console.log("Deleting sponsorId " + sponsorId);
            $.ajax({
                type: "POST",
                url: "deleteSponsor/" + sponsorId,
                success: function (result) {
                    alert(result);
                    location.reload();
                }
            });
        }

        function reloadSponsorDetail(id) {
            console.log("Reloading sponsor detail for #" + id);
            $.getJSON("../sponsor/" + id, function (sponsor) {


                console.log(sponsor[0]); // get the zeroth hotel

                // iterate through JSONObject's keys
                $.each(sponsor[0], function (key, value) {
                    // console.log("key: " + key + " value:" + value);
                    switch (key) {
                        case "sponsor_id":
                            $("#sponsorId").text(value).val(value);
                            return true;
                        case "sponsor_name":
                            $("#sponsorName").text(value).val(value);
                            return true;
                        case "sponsor_image":
                            $("#sponsorImage").text(value).val(value);
                            return true;
                        case "sponsor_description":
                            $("#sponsorDescription").text(value).val(value);
                            return true;
                        case "sponsor_website":
                            $("#sponsorWebsite").text(value).val(value);
                            return true;
                        case "sponsor_address":
                            $("#sponsorAddress").text(value).val(value);
                            return true;
                        case "sponsor_phone":
                            $("#sponsorPhone").text(value).val(value);
                            return true;
                        case "sponsor_email":
                            $("#sponsorEmail").text(value).val(value);
                            return true;
                    }
                });
            });
        }

        // AJAX form posting
        var form = $('#sponsorForm');
        form.submit(function (ev) {

            console.log("Form sent!");
            $.ajax({
                type: form.attr('method'),
                url: form.attr('action'),
                data: new FormData(form),
                success: function (data) {
                    alert(data);
                }
            });

            ev.preventDefault();
        });

        $('#addSponsor').click(function (ev) {
            console.log("Add sponsor clicked!");
            $.ajax({
                type: "POST",
                url: "addSponsor",
                success: function (result) {
                    alert(result);
                    location.reload();
                }
            });
        })
    </script>
@endsection
