@extends('cms.master')

@section('content')
    <div class="col-md-4">
        <h3 class="page-header">Speaker List</h3>
        <p id="speakerList">Loading...</p>
        <button type="button" class="btn btn-primary" id="addSpeaker">Add Speaker</button>
    </div>
    <div class="col-md-8">
        <h3 class="page-header">Speaker Detail</h3>

        {{ Form::open(['action' => 'CmsController@updateSpeaker', 'files' => 'true', 'id' => 'speakerForm', 'class' => 'form-horizontal']) }}

        <div class="form-group">
            <label class="sr-only col-sm-2 control-label">ID</label>
        </div>

        {{-- SPEAKER ID --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">ID</label>
            <div class="col-sm-10">
                <input class="form-control" id="speakerId" name="speakerId">
            </div>
        </div>

        {{-- NAME --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Name</label>
            <div class="col-sm-10">
                <input class="form-control" id="speakerName" name="speakerName">
            </div>
        </div>

        {{-- DESCRIPTION --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Description</label>
            <div class="col-sm-10">
                <input class="form-control" id="speakerDescription" name="speakerDescription">
            </div>
        </div>

        {{-- NATIONALITY --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Nationality</label>
            <div class="col-sm-10">
                <input class="form-control" id="speakerNationality" name="speakerNationality">
            </div>
        </div>

        {{-- IMAGE --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Image</label>
            <div class="col-sm-10">
                <input class="form-control" id="speakerImage" name="speakerImage">
            </div>
        </div>

        {{-- UPLOAD IMAGE --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">{!! Form::label('upload', 'Upload') !!}</label>
            <div class="col-sm-10">
                {!! Form::file('upload') !!}
            </div>
        </div>

        {{-- TITLE (jabatan) --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Title</label>
            <div class="col-sm-10">
                <input class="form-control" id="speakerTitle" name="speakerTitle">
            </div>
        </div>


        {{-- BUTTON TO SEND THE FORM AND UPDATE SPEAKER --}}
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Update Speaker Detail</button>
            </div>
        </div>

        {{ Form::close() }}

    </div>
@endsection

@section('script')
    <script>

        $('#speakerSidebarItem').addClass("active");

        // Fetch and show list of speakers
        $.getJSON("../speaker/all", function (speaker) {
            console.log("Getting all speaker...");
            var speakerListHTMLBody = "";
            $.each(speaker, function (index, element) {
                console.log("Speaker id: " + index + ", name " + element.name);
                speakerListHTMLBody += "<li>" +
                        "<a href='#' onclick='reloadSpeakerDetail(" + this.id + ")'>" + this.name + "</a> " +
                        "<a href='#' onclick='deleteSpeaker(" + this.id + ")'>(Delete)</a>" +
                        "</li>";
            });
            $("#speakerList").html("<ol>" + speakerListHTMLBody + "</ol>");
            reloadSpeakerDetail(1);
        });

        function deleteSpeaker(speakerId) {
            console.log("Deleting speaker with id " + speakerId);
            $.ajax({
                type: "POST",
                url: "deleteSpeaker/" + speakerId,
                success: function (result) {
                    alert(result);
                    location.reload();
                }
            });
        }


        function reloadSpeakerDetail(id) {
            console.log("Reloading speaker detail for #" + id);
            $.getJSON("../speaker/" + id, function (speaker) {

                console.log(speaker[0]); // get the zeroth hotel

                // iterate through JSONObject's keys
                $.each(speaker[0], function (key, value) {
                    // console.log("key: " + key + " value:" + value);
                    switch (key) {
                        case "speaker_id":
                            $("#speakerId").text(value).val(value);
                            return true;
                        case "speaker_name":
                            $("#speakerName").text(value).val(value);
                            return true;
                        case "speaker_description":
                            $("#speakerDescription").text(value).val(value);
                            return true;
                        case "speaker_nationality":
                            $("#speakerNationality").text(value).val(value);
                            return true;
                        case "speaker_image":
                            $("#speakerImage").text(value).val(value);
                            return true;
                        case "speaker_title":
                            $("#speakerTitle").text(value).val(value);
                            return true;
                    }
                });
            });
        }

        // AJAX form posting
        var form = $('#speakerForm');
        form.submit(function (ev) {

            console.log("Form sent!");
            $.ajax({
                type: form.attr('method'),
                url: form.attr('action'),
                data: new FormData(form),
                success: function (data) {
                    alert(data);
                }
            });

            ev.preventDefault();
        });

        $('#addSpeaker').click(function (ev) {
            console.log("Add speaker clicked!");
            $.ajax({
                type: "POST",
                url: "addSpeaker",
                success: function (result) {
                    alert(result);
                    location.reload();
                }
            });
        })

    </script>
@endsection
