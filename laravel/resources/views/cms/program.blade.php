@extends('cms.master')

@section('content')
    <div class="col-md-4">
        <h3 class="page-header">Program List</h3>
        <p id="programList">Loading...</p>
        <button type="button" class="btn btn-primary" id="addProgram">Add Program</button>
    </div>
    <div class="col-md-8">
        <h3 class="page-header">Program Detail</h3>
        {{ Form::open(['action' => 'CmsController@updateProgram', 'files' => 'true', 'id' => 'programForm', 'class' => 'form-horizontal']) }}
        <div class="form-group">
            <label class="sr-only col-sm-2 control-label">ID</label>
        </div>

        {{-- PROGRAM ID --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">ID</label>
            <div class="col-sm-10">
                <input class="form-control" id="programId" name="programId">
            </div>
        </div>

        {{-- PROGRAM NAME --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Name</label>
            <div class="col-sm-10">
                <input class="form-control" id="programName" name="programName">
            </div>
        </div>

        {{-- PROGRAM IMAGE --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Image</label>
            <div class="col-sm-10">
                <input class="form-control" id="programImage" name="programImage">
            </div>
        </div>

        {{-- UPLOAD IMAGE --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">{!! Form::label('upload', 'Upload') !!}</label>
            <div class="col-sm-10">
                {!! Form::file('upload') !!}
            </div>
        </div>

        {{-- PROGRAM DESCRIPTION --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Description</label>
            <div class="col-sm-10">
                <input class="form-control" id="programDescription" name="programDescription">
            </div>
        </div>

        {{-- PROGRAM WEBSITE --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Website</label>
            <div class="col-sm-10">
                <input class="form-control" id="programWebsite" name="programWebsite">
            </div>
        </div>

        {{-- PROGRAM PHONE --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Phone</label>
            <div class="col-sm-10">
                <input class="form-control" id="programPhone" name="programPhone">
            </div>
        </div>

        {{-- PROGRAM EMAIL --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
                <input class="form-control" id="programEmail" name="programEmail">
            </div>
        </div>

        {{-- BUTTON TO SEND THE FORM AND UPDATE PROGRAM --}}
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Update Program Detail</button>
            </div>
        </div>
        {{ Form::close() }}
    </div>
@endsection

@section('script')
    <script>

        $('#programSidebarItem').addClass("active");

        // Fetch and show list of programs
        $.getJSON("../program/all", function (program) {
            console.log("Getting all programs...");
            var programListHTMLBody = "";
            $.each(program, function (index, element) {
                console.log("Program id: " + index + ", name " + element.name);
                programListHTMLBody += "<li>" +
                        "<a href='#' onclick='reloadProgramDetail(" + this['id'] + ")'>" + this['name'] + "</a> " +
                        "<a href='#' onclick='deleteProgram(" + this['id'] + ")'>(Delete)</a>" +
                        "</li>";
            });
            $("#programList").html("<ol>" + programListHTMLBody + "</ol>");
            reloadProgramDetail(1);
        });

        // Fetch and show detailed location information
        function reloadProgramDetail(id) {
            console.log("Reloading program detail for #" + id);
            $.getJSON("../program/" + id, function (program) {

                console.log(program[0]); // get the zeroth hotel

                // iterate through JSONObject's keys
                $.each(program[0], function (key, value) {
                    // console.log("key: " + key + " value:" + value);
                    switch (key) {
                        case "id":
                            $("#programId").text(value).val(value);
                            return true;
                        case "name":
                            $("#programName").text(value).val(value);
                            return true;
                        case "image":
                            $("#programImage").text(value).val(value);
                            return true;
                        case "description":
                            $("#programDescription").text(value).val(value);
                            return true;
                        case "website":
                            $("#programWebsite").text(value).val(value);
                            return true;
                        case "phone":
                            $("#programPhone").text(value).val(value);
                            return true;
                        case "email":
                            $("#programEmail").text(value).val(value);
                            return true;
                    }
                });
            });
        }

        function deleteProgram(programId) {
            console.log("Deleting programId " + programId);
            $.ajax({
                type: "POST",
                url: "deleteProgram/" + programId,
                success: function (result) {
                    alert(result);
                    location.reload();
                }
            });
        }

        // AJAX form posting
        var form = $('#programForm');
        form.submit(function (ev) {

            console.log("Form sent!");
            $.ajax({
                type: form.attr('method'),
                url: form.attr('action'),
                data: new FormData(form),
                success: function (data) {
                    alert(data);
                }
            });

            ev.preventDefault();
        });

        $('#addProgram').click(function (ev) {
            console.log("Add program clicked!");
            $.ajax({
                type: "POST",
                url: "addProgram",
                success: function (result) {
                    alert(result);
                    location.reload();
                }
            });
        })
    </script>
@endsection
