@extends('cms.master')

@section('content')
    <h1 class="page-header">Petunjuk</h1>

    <div class="row">
        <div class="col-xs-8 col-sm-8 placeholder">
            <p>Di sebelah kiri, Anda bisa memilih beberapa menu sekaligus mengedit isi data yang akan ditampilkan di
                aplikasi mobile.</p>
        </div>
    </div>

    <h2 class="sub-header">Latest Data</h2>
@endsection
@section('script')
    <script>
        $('#dashboardSidebarItem').addClass("active");
    </script>
@endsection
