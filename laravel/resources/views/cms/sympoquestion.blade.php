@extends('cms.master')

@section('content')
    <div class="col-md-3">
        <h3 class="page-header">Specialization</h3>
        <p id="specializationList">(ada empat spesialisasi)</p>
    </div>
    <div class="col-md-3">
        <h3 class="page-header" id="specName">Symposium</h3>
        <p id="symposiumList">Loading...</p>
    </div>
    <div class="col-md-6">
        <h3 class="page-header">Symposium Question</h3>
        <p id="questionList">Loading...</p>
    </div>
@endsection

@section('script')
    <script>

        $('#sympoQuestionSidebarItem').addClass("active");

        // Fetch and show list of specializations
        $.getJSON("../symposium/specialization", function (specializations) {
            console.log("Getting all specialization...");
            var specializationListHTMLBody = "";
            $.each(specializations, function (index, element) {
                console.log("SYMPOSIUM id: " + index + ", name " + element.name + ", eventId = " + element.id);
                specializationListHTMLBody += "<li>" +
                        "<a href='#' onclick='reloadSymposiumList(" + this['id'] + ")'>" + this['name'] + "</a>" +
                        "</li>";
            });
            $("#specializationList").html("<ol>" + specializationListHTMLBody + "</ol>");
            reloadSymposiumList(specializations[0].id); // hardcoded wkwkwkwk
        });

        // Fetch and show list of all symposiums for a given specialization
        function reloadSymposiumList(specializationId) {

            selectedSpecializationId = specializationId;

            // changing name for specName header
            console.log("changing name...");
            $.getJSON("../symposium/specialization/" + specializationId, function (specialization) {
                $("#specName").text(specialization[0].symspec_name);
            });

            $.getJSON("../symposium/getAllForSpec/" + specializationId, function (symposiums) {
                console.log("Getting all specialization for specialization " + specializationId + "...");
                var symposiumListHTMLBody = "";
                $.each(symposiums, function (index, element) {
                    console.log("SYMPOSIUM id: " + index + ", name " + element.name + ", eventId = " + element.id);
                    symposiumListHTMLBody += "<li>" +
                            "<a href='#' onclick='reloadSymposiumQuestion(" + this['id'] + ")'>" + this['name'] + "</a> " +
                            "</li>";
                });
                $("#symposiumList").html("<ol>" + symposiumListHTMLBody + "</ol>");
                reloadSymposiumQuestion(symposiums[0].id);
            });
        }

        function reloadSymposiumQuestion(symposiumId) {

            selectedSymposiumId = symposiumId;

            console.log("Fetching questionList for id=" + symposiumId);
            $.getJSON("../question/sympo/get/" + symposiumId, function (question) {

                var workshopListHTMLBody = "";
                $.each(question, function (index, item) {
                    console.log("QSTIN id: " + index + ", name " + item.question_asker + ", eventId = " + item.id);
                    workshopListHTMLBody += "<li>" +
                            "(" + this.question_asker + ", " + this.question_time + ") " + this.question_content +
                            "</li>";
                });
                $("#questionList").html("<ol>" + workshopListHTMLBody + "</ol>");
            });
        }
    </script>
@endsection
