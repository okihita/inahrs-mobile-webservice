<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use DB;

class SponsorController extends Controller
{
    public function getSponsorList()
    {
        $events = DB::table('sponsor')->select(
            'sponsor_id as id',
            'sponsor_name as name',
            'sponsor_image as image'
        )->get();
        return $events;
    }

    public function getSponsor($id)
    {
        $events = DB::table('sponsor')->where('sponsor_id', $id)->get();
        return $events;
    }
}