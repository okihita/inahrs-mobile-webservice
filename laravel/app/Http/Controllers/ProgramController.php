<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use DB;

class ProgramController extends Controller
{
    public function getProgramList()
    {
        $hotels = DB::table('program')
            ->select(
                'program_id as id',
                'program_name as name',
                'program_image as image'
            )
            ->get();

        return $hotels;
    }

    public function getProgram($id)
    {
        $hotel = DB::table('program')
            ->select(
                'program_id as id',
                'program_name as name',
                'program_image as image',
                'program_description as description',
                'program_website as website',
                'program_phone as phone',
                'program_email as email'
            )
            ->where('program_id', $id)->get();
        return $hotel;
    }
}
