<?php

namespace App\Http\Controllers;

use DB;

class SymposiumController extends Controller
{
    public function getSpecializationList()
    {
        $specializations = DB::table('symposium_specialization')
            ->select(
                'symspec_id as id',
                'symspec_name as name'
            )
            ->get();

        return $specializations;
    }

    public function getAllForSpec($specializationId)
    {
        $specializations = DB::table('symposium')
            ->select(
                'symposium_id as id',
                'symposium_specialization_id as specialization_id',
                'symposium_name as name',
                'symposium_image as image',
                'symposium_description as description',
                'symposium_date as date',
                'symposium_start_time as start_time',
                'symposium_end_time as end_time',
                'symposium_venue as venue'
            )
            ->where('symposium_specialization_id', $specializationId)
            ->get();

        return $specializations;
    }

    public function getSymposium($symposiumId)
    {
        $hotel = DB::table('symposium')->where('symposium_id', $symposiumId)->get();
        return $hotel;
    }

    public function getSpecialization($specializationId)
    {
        $hotel = DB::table('symposium_specialization')->where('symspec_id', $specializationId)->get();
        return $hotel;
    }
}
