<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\DB;

class HotelController extends Controller
{
    public function getHotelList()
    {
        $hotels = DB::table('hotel')
            ->select(
                'hotel_id as id',
                'hotel_name as name',
                'hotel_image as image',
                'hotel_distance as distance')
            ->get();

        return $hotels;
    }

    public function getHotel($id)
    {
        $hotel = DB::table('hotel')->where('hotel_id', $id)->get();
        return $hotel;
    }
}
