<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Response;

//1. Program
//2. Symposium
//3. Workshop
//4. Question
//5. Speaker
//6. Sponsor
//7. Hotel

class CmsController extends Controller
{
    /* Welcome page. */
    public function welcome()
    {
        return view('welcome');
    }

    /* If user's logged in, go to dashboard. If not, show login form. */

    // UNIVERSAL ACCESS
    public function getLogin()
    {
        session_start();
        if (isset($_SESSION["username"])) {
            return redirect('cms/dashboard');
        } else {
            return view('cms.login');
        }
    }

    public function postLogin()
    {
        $email = Input::get('email');
        $password = Input::get('password');
        if ($email == "admin@admin.com" and $password == "admin") {
            session_start();
            $_SESSION["username"] = "Okihita";

            // If login is correct
            return redirect('cms/dashboard');
        } else {
            // If login is correct
            return redirect('cms/login')->with('status', 'Login failed');
        }
    }

    public function dashboard()
    {
        session_start();
        if (!isset($_SESSION["username"])) {
            return redirect('cms/login');
        } else {
            return view('cms.dashboard');
        }
    }

    public function getLogout()
    {
        session_start();
        session_unset();
        session_destroy();
        return redirect('cms');
    }

    public function uploadAndReturnPath(Request $request)
    {
        $name = $request;
    }

    // PROGRAM
    public function program()
    {
        session_start();
        /* If not logged in, return to login. */
        if (!isset($_SESSION["username"])) {
            return redirect('cms/login');
        } else {
            return view('cms.program');
        }
    }

    public function getAllPrograms()
    {
        $programs = DB::table('program')
            ->select(
                'program_id as id',
                'program_name as name',
                'program_image as image',
                'program_description as description',
                'program_website as website',
                'program_phone as description',
                'program_email as email'
            )
            ->get();

        return $programs;
    }

    public function updateProgram(Request $request)
    {
        try {

            if (Input::hasFile('upload')) {
                Input::file('upload')->getFilename();
                $destinationPath = 'imageuploads'; // upload path
                $extension = Input::file('upload')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(000000, 999999) . '.' . $extension; // renameing image
                Input::file('upload')->move($destinationPath, $fileName); // uploading file to given path

                $programImageInput = $destinationPath . "/" . $fileName;
            } else {
                $programImageInput = $request->input('programImage');
            }


            DB::table('program')
                ->where('program_id', Input::get('programId'))
                ->update([
                    'program_name' => Input::get('programName'),
                    'program_image' => $programImageInput,
                    'program_description' => Input::get('programDescription'),
                    'program_website' => Input::get('programWebsite'),
                    'program_phone' => Input::get('programPhone'),
                    'program_email' => Input::get('programEmail')
                ]);

            return "Successfully updated program " . Input::get('programName') . "'s detail.
            <a href='program'>Return to program list.</a>";
        } catch (\Exception $anything) {
            return "Failed: " . $anything;
        }
    }

    public function addProgram()
    {
        try {
            DB::table('program')
                ->insert(['program_name' => "New Program"]);

            return "Successfully added new program.";
        } catch (\Exception $anything) {
            return "Failed: " . $anything;
        }
    }

    public function deleteProgram($programId)
    {
        try {
            DB::table('program')
                ->where('program_id', $programId)
                ->delete();

            return "Successfully deleted the program id: " . $programId;
        } catch (\Exception $anything) {
            return "Failed: " . $anything;
        }
    }

    // SYMPOSIUM
    public function symposium()
    {
        session_start();
        /* If not logged in, return to login. */
        if (!isset($_SESSION["username"])) {
            return redirect('cms/login');
        } else {
            return view('cms.symposium');
        }
    }

    public function getAllSymposium()
    {
        $EVENT_TYPE_SYMPOSIUM = 1;

        $symposiums = DB::table('event')
            ->select(
                'event_id as id',
                'event_name as name',
                'event_image as image',
                'event_description as description',
                'event_date as date',
                'event_start_time as start_time',
                'event_end_time as end_time',
                'event_venue as venue'
            )
            ->where('event_type', $EVENT_TYPE_SYMPOSIUM)
            ->get();

        return $symposiums;
    }

    public function updateSymposium(Request $request)
    {
        try {

            if (Input::hasFile('upload')) {
                Input::file('upload')->getFilename();
                $destinationPath = 'imageuploads'; // upload path
                $extension = Input::file('upload')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(000000, 999999) . '.' . $extension; // renameing image
                Input::file('upload')->move($destinationPath, $fileName); // uploading file to given path

                $sympoImageInput = $destinationPath . "/" . $fileName;
            } else {
                $sympoImageInput = $request->input('symposiumImage');
            }

            DB::table('symposium')
                ->where('symposium_id', Input::get('symposiumId'))
                ->update([
                    'symposium_name' => Input::get('symposiumName'),
                    'symposium_image' => $sympoImageInput,
                    'symposium_description' => Input::get('symposiumDescription'),
                    'symposium_date' => Input::get('symposiumDate'),
                    'symposium_start_time' => Input::get('symposiumStartTime'),
                    'symposium_end_time' => Input::get('symposiumEndTime'),
                    'symposium_venue' => Input::get('symposiumVenue')
                ]);

            return "Successfully updated symposium " . Input::get('symposiumName') . "'s detail.
            <a href='symposium'>Return to symposium list.</a>";
        } catch (\Exception $anything) {
            return "Failed: " . $anything;
        }
    }

    public function addSymposiumForSpec($specializationId)
    {
        try {
            DB::table('symposium')
                ->insert([
                    'symposium_specialization_id' => $specializationId,
                    'symposium_name' => "New Workshop",
                ]);

            return "Successfully added new workshop.";
        } catch (\Exception $anything) {
            return "Failed: " . $anything;
        }
    }

    public function deleteSymposium($symposiumId)
    {
        try {
            DB::table('symposium')
                ->where('symposium_id', $symposiumId)
                ->delete();

            return "Successfully deleted the workshop id: " . $symposiumId;
        } catch (\Exception $anything) {
            return "Failed: " . $anything;
        }
    }

    // WORKSHOP
    public function workshop()
    {
        session_start();
        /* If not logged in, return to login. */
        if (!isset($_SESSION["username"])) {
            return redirect('cms/login');
        } else {
            return view('cms.workshop');
        }
    }

    public function getAllWorkshop()
    {

        $workshops = DB::table('workshop')
            ->select(
                'workshop_id as id',
                'workshop_name as name',
                'workshop_image as image',
                'workshop_description as description',
                'workshop_date as date',
                'workshop_start_time as start_time',
                'workshop_end_time as end_time',
                'workshop_venue as venue'
            )
            ->get();

        return $workshops;
    }

    public function updateWorkshop(Request $request)
    {
        try {

            if (Input::hasFile('upload')) {
                Input::file('upload')->getFilename();
                $destinationPath = 'imageuploads'; // upload path
                $extension = Input::file('upload')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(000000, 999999) . '.' . $extension; // renameing image
                Input::file('upload')->move($destinationPath, $fileName); // uploading file to given path

                $workshopImageInput = $destinationPath . "/" . $fileName;
            } else {
                $workshopImageInput = $request->input('workshopImage');
            }

            DB::table('workshop')
                ->where('workshop_id', Input::get('workshopId'))
                ->update([
                    'workshop_name' => Input::get('workshopName'),
                    'workshop_image' => $workshopImageInput,
                    'workshop_description' => Input::get('workshopDescription'),
                    'workshop_date' => Input::get('workshopDate'),
                    'workshop_start_time' => Input::get('workshopStartTime'),
                    'workshop_end_time' => Input::get('workshopEndTime'),
                    'workshop_venue' => Input::get('workshopVenue')
                ]);

            return "Successfully updated workshop " . Input::get('workshopName') . "'s detail.
            <a href='workshop'>Return to workshop list.</a>";
        } catch (\Exception $anything) {
            return "Failed: " . $anything;
        }
    }

    public function addWorkshop()
    {
        try {
            DB::table('workshop')
                ->insert(['workshop_name' => "New Workshop"]);

            return "Successfully added new workshop.";
        } catch (\Exception $anything) {
            return "Failed: " . $anything;
        }
    }

    public function deleteWorkshop($workshopId)
    {
        try {
            DB::table('workshop')
                ->where('workshop_id', $workshopId)
                ->delete();

            return "Successfully deleted the workshop id: " . $workshopId;
        } catch (\Exception $anything) {
            return "Failed: " . $anything;
        }
    }

    // QUESTION
    public function workshopQuestion()
    {
        session_start();
        /* If not logged in, return to login. */
        if (!isset($_SESSION["username"])) {
            return redirect('cms/login');
        } else {
            return view('cms.workshopquestion');
        }
    }

    public function sympoQuestion()
    {
        session_start();
        /* If not logged in, return to login. */
        if (!isset($_SESSION["username"])) {
            return redirect('cms/login');
        } else {
            return view('cms.sympoquestion');
        }
    }

    // SPEAKER
    public function speaker()
    {
        session_start();
        /* If not logged in, return to login. */
        if (!isset($_SESSION["username"])) {
            return redirect('cms/login');
        } else {
            return view('cms.speaker');
        }
    }

    public function updateSpeaker(Request $request)
    {
        try {

            if (Input::hasFile('upload')) {
                Input::file('upload')->getFilename();
                $destinationPath = 'imageuploads'; // upload path
                $extension = Input::file('upload')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(000000, 999999) . '.' . $extension; // renameing image
                Input::file('upload')->move($destinationPath, $fileName); // uploading file to given path

                $speakerImageInput = $destinationPath . "/" . $fileName;
            } else {
                $speakerImageInput = $request->input('hotelImage');
            }


            DB::table('speaker')
                ->where('speaker_id', Input::get('speakerId'))
                ->update([
                    'speaker_name' => Input::get('speakerName'),
                    'speaker_description' => Input::get('speakerDescription'),
                    'speaker_nationality' => Input::get('speakerNationality'),
                    'speaker_image' => $speakerImageInput,
                    'speaker_title' => Input::get('speakerTitle')
                ]);

            return "Successfully updated speaker " . Input::get('speakerName') . "'s detail.
<a href='speaker'>Return to speaker list.</a>";
        } catch (\Exception $anything) {
            return "Failed: " . $anything;
        }
    }

    public function addSpeaker()
    {
        try {
            DB::table('speaker')
                ->insert(['speaker_name' => "New Speaker"]);

            return "Successfully added new speaker.";
        } catch (\Exception $anything) {
            return "Failed: " . $anything;
        }
    }

    public function deleteSpeaker($speakerId)
    {
        try {
            DB::table('speaker')
                ->where('speaker_id', $speakerId)
                ->delete();

            return "Successfully deleted the sponsor id " . $speakerId;
        } catch (\Exception $anything) {
            return "Failed: " . $anything;
        }
    }

    // SPONSOR
    public function sponsor()
    {
        session_start();
        /* If not logged in, return to login. */
        if (!isset($_SESSION["username"])) {
            return redirect('cms/login');
        } else {
            return view('cms.sponsor');
        }
    }

    public function updateSponsor(Request $request)
    {
        try {

            if (Input::hasFile('upload')) {
                Input::file('upload')->getFilename();
                $destinationPath = 'imageuploads'; // upload path
                $extension = Input::file('upload')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(000000, 999999) . '.' . $extension; // renameing image
                Input::file('upload')->move($destinationPath, $fileName); // uploading file to given path

                $sponsorImageInput = $destinationPath . "/" . $fileName;
            } else {
                $sponsorImageInput = $request->input('sponsorImage');
            }

            DB::table('sponsor')
                ->where('sponsor_id', Input::get('sponsorId'))
                ->update([
                    'sponsor_name' => Input::get('sponsorName'),
                    'sponsor_image' => $sponsorImageInput,
                    'sponsor_description' => Input::get('sponsorDescription'),
                    'sponsor_website' => Input::get('sponsorWebsite'),
                    'sponsor_address' => Input::get('sponsorAddress'),
                    'sponsor_phone' => Input::get('sponsorPhone'),
                    'sponsor_email' => Input::get('sponsorEmail')
                ]);

            return "Successfully updated sponsor " . Input::get('sponsorName') . "'s detail.
<a href='sponsor'>Return to sponsor list.</a>";
        } catch (\Exception $anything) {
            return "Failed: " . $anything;
        }
    }

    public function addSponsor()
    {
        try {
            DB::table('sponsor')
                ->insert(['sponsor_name' => "New Sponsor"]);

            return "Successfully added new sponsor.";
        } catch (\Exception $anything) {
            return "Failed: " . $anything;
        }
    }

    public function deleteSponsor($sponsorId)
    {
        try {
            DB::table('sponsor')
                ->where('sponsor_id', $sponsorId)
                ->delete();

            return "Successfully deleted the sponsor id " . $sponsorId;
        } catch (\Exception $anything) {
            return "Failed: " . $anything;
        }
    }

    // HOTEL
    public function hotel()
    {
        session_start();
        /* If not logged in, return to login. */
        if (!isset($_SESSION["username"])) {
            return redirect('cms/login');
        } else {
            return view('cms.hotel');
        }
    }

    public function getAllHotels()
    {
        // $hotels = DB::table('hotel')->get();
        $hotels = DB::table('hotel')
            ->select(
                'hotel_id as id',
                'hotel_name as name',
                'hotel_image as image',
                'hotel_price as price',
                'hotel_detail as detail',
                'hotel_lat as lat',
                'hotel_lng as lng',
                'hotel_alamat as alamats',
                'hotel_telepon as telepon',
                'hotel_distance as distance',
                'hotel_website as website')
            ->get();

        return $hotels;
    }

    public function updateHotel(Request $request)
    {
        try {

            if (Input::hasFile('upload')) {
                Input::file('upload')->getFilename();
                $destinationPath = 'imageuploads'; // upload path
                $extension = Input::file('upload')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(000000, 999999) . '.' . $extension; // renameing image
                Input::file('upload')->move($destinationPath, $fileName); // uploading file to given path

                $hotelImageInput = $destinationPath . "/" . $fileName;
            } else {
                $hotelImageInput = $request->input('hotelImage');
            }

            DB::table('hotel')
                ->where('hotel_id', $request->input('hotelId'))
                ->update([
                    'hotel_name' => $request->input('hotelName'),
                    'hotel_image' => $hotelImageInput,
                    'hotel_price' => $request->input('hotelPrice'),
                    'hotel_detail' => $request->input('hotelDetail'),
                    'hotel_lat' => $request->input('hotelLat'),
                    'hotel_lng' => $request->input('hotelLng'),
                    'hotel_alamat' => $request->input('hotelAlamat'),
                    'hotel_telepon' => $request->input('hotelTelepon'),
                    'hotel_distance' => $request->input('hotelDistance'),
                    'hotel_website' => $request->input('hotelWebsite')
                ]);

            return "Successfully updated hotel " . $request->input('hotelName') . "'s detail.
<a href='hotel'>Return to hotel list.</a>";

        } catch (\Exception $anything) {
            return "Failed: " . $anything;
        }
    }

    public function addHotel()
    {
        try {
            DB::table('hotel')
                ->insert(['hotel_name' => "New Hotel"]);

            return "Successfully added new hotel.";
        } catch (\Exception $anything) {
            return "Failed: " . $anything;
        }
    }

    public function deleteHotel($hotelId)
    {
        try {
            DB::table('hotel')
                ->where('hotel_id', $hotelId)
                ->delete();

            return "Successfully deleted the hotel id " . $hotelId;
        } catch (\Exception $anything) {
            return "Failed: " . $anything;
        }
    }
}
